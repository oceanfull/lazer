import Vue from 'vue'
import Vuex from 'vuex'
import ru from './locale-ru.json'
import ua from './locale-ua.json'
import prices from './prices.json'

Vue.use(Vuex)

export default new Vuex.Store({
  modules: {
    document: {
      namespaced: true,
      state: {
        height: 0,
        width: 0
      },
      mutations: {
        setDimensions: (state,{height,width}) => {
          state.height = height;
          state.width = width;
        }
      },
      getters: {
        isMobile(state) {
          return state.width < 1026;
        }
      }
    },
    router: {
      namespaced: true,
      state: {
        active: '',
        navbar: ['home:price','home:tech','home:contacts'] //,'home:blog', 'home:discount', 'home:feed'
      },
      mutations: {
        setActive: (state,sectionName) => state.active = sectionName
      }
    },
    sites: {
      namespaced: true,
      state: {
        currentSite: localStorage.getItem('current-site') || "kamyahec"
      },
      getters: {
        getSite(state) {
          return state.currentSite;
        }
      },
      mutations: {
        setSite(state,site) {
          localStorage.setItem('current-site',site)
          state.currentSite = site
        } 
      }
    },
    locales: {
      namespaced: true,
      state: {
        currentLang: localStorage.getItem('lang') || "ru",
        translations: {
          ru,
          ua
        }
      },
      mutations: {
        setLang(state,lang) {
          localStorage.setItem('lang',lang)
          state.currentLang = lang
        } 
      },
      getters: {
        getLocale(state) {
          return state.translations[state.currentLang]
        },
        getCurrentLang(state) {
          return state.currentLang
        }
      }
    },
    prices: {
      namespaced: true,
      state: {
        prices: prices,
      },
      getters: {
        getPrices(state) {
          return state.prices
        },
        getTatooPrices(state) {
          const tatoos = state.prices.tatoo
          const newObj = {};
          for (const key in tatoos) {
            const element = tatoos[key];
            for (const k in element) {
              const e = element[k];
              e.name = key
              if(!newObj[e.size])
              newObj[e.size] = []
              newObj[e.size].push(e);
            }
          }
          const trKeys = Object.keys(state.prices.tatoo);
          return {
            tr: trKeys,
            newObj
          }
        }
      }
    }
  }
})
